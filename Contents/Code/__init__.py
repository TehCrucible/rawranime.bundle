# rawrANIME (BY TEHCRUCIBLE) - v2.00

TITLE = "rawrANIME"
PREFIX = "/video/rawranime"
ART = "art-default.jpg"
ICON = "icon-default.png"
ICON_LIST = "icon-list.png"
ICON_COVER = "icon-cover.png"
ICON_SEARCH = "icon-search.png"
ICON_QUEUE = "icon-queue.png"
BASE_URL = "http://rawranime.tv"

# Menu hierarchy

@handler(PREFIX, TITLE, art=ART, thumb=ICON)
def MainMenu():

    oc = ObjectContainer()

    oc.add(DirectoryObject(
        key = Callback(GetShows, url = BASE_URL + '/index.php?ajax=anime&do=getlist&rs=undefined', title = 'Anime List'),
        title = 'Anime List'
        )
    )

    return oc

@route(PREFIX + '/list')
def GetShows(url, title):

    response = JSON.ObjectFromURL(url)
    data = HTML.ElementFromString(response['html'])

    oc = ObjectContainer(title1 = title)

    for each in data.xpath('//a[@class="al-card"]'):

        url = BASE_URL + each.xpath('./@href')[0]
        title = each.xpath('./div[@class="al-name"]/text()')[0]
        thumb = 'http:' + each.xpath('./div[@class="al-image"]/@data-src')[0]

        show = DirectoryObject()
        show.key = Callback(GetEpisodes, url = url, title = title)
        show.title = title
        show.thumb = Resource.ContentsOfURLWithFallback(url = thumb, fallback='icon-cover.png')

        oc.add(show)

    return oc

@route(PREFIX + '/episodes')
def GetEpisodes(url, title):

    data = HTML.ElementFromURL(url)

    oc = ObjectContainer(title1 = title)

    for each in data.xpath('//div[contains(@class, "ep ")]'):

        number = each.xpath('.//div[@class="ep-title"]/div[@class="ep-number"]/text()')[0]
        name = each.xpath('.//div[@class="ep-title"]/text()[last()]')[0].strip()
        title = number + '. ' + name
        url = BASE_URL + each.xpath('//div[@class="ep-qualities"]/a/@href')[0]

        thumb = ''
        if len(each.xpath('./a[@class="ep-bg"]')) > 0:
            thumb = 'http:' + each.xpath('./a[@class="ep-bg"]/@data-src')[0]

        episode = PopupDirectoryObject()
        episode.key = Callback(GetMirrors, url = url)
        episode.title = title
        episode.thumb = Resource.ContentsOfURLWithFallback(url = thumb, fallback='icon-cover.png')

        oc.add(episode)

    return oc

@route(PREFIX + '/mirrors')
def GetMirrors(url):

    oc = ObjectContainer()

    data = HTML.ElementFromURL(url)

    title = data.xpath('//div[@id="video-title"]/text()')[0].strip()

    for each in data.xpath('//div[contains(@class, "mirror ")]'):

        quality = each.xpath('./@data-quality')[0]
        lang = each.xpath('./@data-lang')[0]
        provider = each.xpath('.//div[@class="mirror-provider"]/text()')[0]

        video = VideoClipObject()
        video.url = each.xpath('./@data-src')[0]
        video.title = title + ' ' + quality + ' ' + lang + ' ' + provider

        oc.add(video)

    return oc